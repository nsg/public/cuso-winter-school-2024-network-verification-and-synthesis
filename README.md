# CUSO Winter School 2024 Network Verification and Synthesis


## Setup

You will need the [z3 solver](https://theory.stanford.edu/~nikolaj/programmingz3.html) and additional python libraries. 

To install these on Linux, just run:

```
sudo apt-get install python3-z3 python3-networkx
```

To install these on MacOS, the simplest way is to use `brew` and `pip`:

```
brew install z3
pip install z3-solver
pip install networkx
```

## Exercises

In this simple exercise, we'll explore how a network verifier works by running it on a simple network.

The network we'll play with is the same as in the slides and is defined in `verification.py`.

The other files are as such:

- `routing_state.py` defines classes capturing the concepts behind a route and a router (consider both internal and external routers);
- `routing_semantics.py` defines functions that generate all the routing equations alongside with a function that transforms a route propagated or advertised from one router to another.
- `tasks.py` contains some example specifications.
- `network.py` defines how to represent the network itself (along with some helper function like pretty printers).

### Step 1: Verify that the current network is incorrect

Execute `verification.py` and verify that the specification is violated. Look at the concrete counter-example and reason about it.

### Step 2: Repair the network using route-maps

Now, fix the network using a route-map. To add a route-map, simply use the function `net.set_route_map(src, dst, RouteMap([RmItem(...), ...]))` from within `verification.py`. Check `route_map.py` for the definition of `RmItem`. While multiple solutions exist, a common one would involve increasing the preference (`local_pref`) of the customer routes learned by `C`.

Execute `verification.py` again to ensure that your fix is correct. If so, it should say "Specification is satisfied!".

### Step 3:  Play with a slightly harder specification

Write a specification that describes the following behavior:

>The network prefers routes from the Customer unless the Provider advertises a route with community 5.

To do so, delete line 48 (`from tasks import Task_1 as Spec`) in `verification.py` and replace it with the skeleton:

```
def Spec(net):
    Customer = net.Routers["Customer"]
    Provider = net.Routers["Provider"]
    A = net.Routers["A"]
    B = net.Routers["B"]
    C = net.Routers["C"]
    D = net.Routers["D"]

    return If(
		<COMPLETE_ME>
    )
````

Then, design a configuration that satisfies the specification. Use the counter-examples from the verifier to find a valid configuration.

*Hint*: Ensure you cover all cases (e.g., what happens if the Customer advertises a route but not the Provider?).